<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class Movie extends Controller
{
	public function index()
	{
		$parent = ['id' => '0'];
        $this->assign('parent', $parent);

		$movie=Db::name('ysmovie')->order('sorts asc')->select();//电影
        $this->assign('movie',$movie);
        $dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);

        return $this->fetch();//这里  都return了
	}

}


?>