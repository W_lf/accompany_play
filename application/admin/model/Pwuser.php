<?php
namespace app\admin\model;

use think\Model;

class Pwuser extends Model
{
    protected $table = "tf_accompany" ;



    protected $insert  = ['logins', 'reg_ip', 'last_time', 'last_ip'];
    //protected $update = [];

    public function userInfo()
    {
        /**
         * HAS ONE 关联定义
         * @access public
         * @param string $model      模型名
         * @param string $foreignKey 关联外键
         * @param string $localKey   关联主键
         * @param array  $alias      别名定义（已经废弃）
         * @param string $joinType   JOIN类型
         */
        return $this->hasOne('userInfo', 'uid', 'id');
    }

    public function userGroup()
    {
        /**
         * HAS MANY 关联定义
         * @access public
         * @param string $model      模型名
         * @param string $foreignKey 关联外键
         * @param string $localKey   关联主键
         * @return HasMany
         */
        return $this->hasMany('authGroupAccess', 'uid', 'id');
    }

    protected function setPasswordAttr($value)
    {
        /**
         * Calculate the md5 hash of a string
         * @link https://php.net/manual/en/function.md5.php
         * @param string $str <p>
         * The string.
         * </p>
         * @param bool $raw_output [optional] <p>
         * If the optional raw_output is set to true,
         * then the md5 digest is instead returned in raw binary format with a
         * length of 16.
         * </p>
         * @return string the hash as a 32-character hexadecimal number.
         * @since 4.0
         * @since 5.0
         */
        return md5($value);
    }
    protected function setLoginsAttr()
    {
        return '0';//返回值为0
    }
    protected function setRegIpAttr()
    {
        /**
         * 获取当前Request对象实例
         * @return Request
         */
        /**
         * 获取客户端IP地址
         * @param integer   $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
         * @param boolean   $adv 是否进行高级模式获取（有可能被伪装）
         * @return mixed
         */
        return request()->ip();
    }
    protected function setLastTimeAttr()
    {
        return time();
    }
    protected function setLastIpAttr()
    {
        /**
         * 获取当前Request对象实例
         * @return Request
         */
        /**
         * 获取客户端IP地址
         * @param integer   $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
         * @param boolean   $adv 是否进行高级模式获取（有可能被伪装）
         * @return mixed
         */
        return request()->ip();
    }

    public function getStatusTurnAttr($value, $data)
    {
        $turnArr = [0=>lang('status0'), 1=>lang('status1')];
        return $turnArr[$data['status']];
    }
    public function getLastTimeTurnAttr($value, $data)
    {
        return date('Y-m-d H:i:s', $data['last_time']);
    }
}