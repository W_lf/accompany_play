<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class Jingdian extends Controller
{
	public function index()
	{
		$parent = ['id' => '0'];
        $this->assign('parent', $parent);
		$dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);

		$jd=Db::name('ysjingdian')->join('gm_ysjdtype','gm_ysjingdian.tid=gm_ysjdtype.id')->field('gm_ysjingdian.id,gm_ysjingdian.title,gm_ysjingdian.text,gm_ysjingdian.url,gm_ysjingdian.pic,gm_ysjingdian.ctime,gm_ysjingdian.sorts,gm_ysjdtype.type')->order('sorts asc')->select();//经典影视
        $this->assign('jd',$jd);

        return $this->fetch();//这里  都return了
	}

}


?>