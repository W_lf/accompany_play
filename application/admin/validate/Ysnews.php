<?php
namespace app\admin\validate;

use think\Validate;

class Ysnews extends Validate
{
    protected $rule = [//require是必填
        'title' => 'require',
        'author' => 'require',
        'text' => 'require',
        'pic' => 'require',
        'url' => 'require',
        'ctime' => 'require',
        'utime' => 'require',
        'sorts' => 'require|integer|>=:1',//integer是整数 后面是大于等于1的意思 以后排序字段最好用sorts
    ];

    protected $message = [
        'title' => '{%title_val}',
        'author' => '{%title_val}',
        'text' => '{%text_val}',
        'pic' => '{%pic_val}',
        'url' => '{%url_val}',
        'ctime' => '{%ctime_val}',
        'utime' => '{%ctime_val}',
        'sorts' => '{%stor_val}',
    ];

    protected $scene = [
        /*对应这个**/'add'   => ['title', 'text', 'author', 'pic', 'url', 'ctime', 'utime', 'sorts'],//意思就是点击增加时候 走验证类 验证这个 里面有这四个字段 然后去找到 上面的$rule去验证规则 验证不过的提示$message
        'edit'  => ['title', 'text', 'author', 'pic', 'url', 'ctime', 'utime', 'sorts'],
        'status' => ['status'],
    ];
}