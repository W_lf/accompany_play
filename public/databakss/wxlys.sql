/*
SQLyog v10.2 
MySQL - 8.0.12 : Database - yingshi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `gm_addonalbum` */

DROP TABLE IF EXISTS `gm_addonalbum`;

CREATE TABLE `gm_addonalbum` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aid` int(11) NOT NULL COMMENT '关联文章ID',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `gm_addonalbum` */

insert  into `gm_addonalbum`(`id`,`aid`,`content`) values (1,3,'相册模型1');

/*Table structure for table `gm_addonarticle` */

DROP TABLE IF EXISTS `gm_addonarticle`;

CREATE TABLE `gm_addonarticle` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aid` int(11) NOT NULL COMMENT '关联文章ID',
  `content` text COMMENT '内容',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `gm_addonarticle` */

insert  into `gm_addonarticle`(`id`,`aid`,`content`) values (1,1,'tf_bill --- 【XX】账单信息\n&lt;p&gt;\n	tf_collection --- 【XX】收藏\n&lt;/p&gt;\n&lt;p&gt;\n	tf_order --- 【XX】订单\n&lt;/p&gt;\n&lt;p&gt;\n	tf_withdraw --- 【XX】提现\n&lt;/p&gt;\n&lt;p&gt;\n	图片惰性加载jquery.lazyload.min.js\n&lt;/p&gt;\n&lt;p&gt;\n	&lt;br /&gt;\n&lt;/p&gt;\n&lt;p&gt;\n	jquery.scrollLoading\n&lt;/p&gt;'),(2,2,''),(3,3,'只有简介，没有图'),(4,4,'&lt;p&gt;\n	&lt;br /&gt;\n&lt;/p&gt;'),(5,5,'所谓缘分，就是遇见了该遇见的人'),(6,6,'每一次遗憾都是对生命的一种肯定'),(7,7,'&lt;p&gt;\n	以为我永远只能一个人生活\n&lt;/p&gt;\n&lt;p&gt;\n	孤单地快乐哀愁\n&lt;/p&gt;\n&lt;p&gt;\n	偶尔可以伪装潇洒自由\n&lt;/p&gt;\n&lt;p&gt;\n	心里慌了起来自己喝酒\n&lt;/p&gt;\n&lt;p&gt;\n	以为你只是一个美丽的偶然\n&lt;/p&gt;\n&lt;p&gt;\n	垂怜我不经意降落\n&lt;/p&gt;\n&lt;p&gt;\n	谁知道你不同 谁知道你不走\n&lt;/p&gt;\n&lt;p&gt;\n	拥抱着我说终于找到了我\n&lt;/p&gt;\n&lt;p&gt;\n	你 看穿我的冷漠 亲吻我的烦忧\n&lt;/p&gt;\n&lt;p&gt;\n	不在乎我曾经的错\n&lt;/p&gt;\n&lt;p&gt;\n	如果不是你的款款温柔\n&lt;/p&gt;\n&lt;p&gt;\n	还以为真爱 只是一个传说\n&lt;/p&gt;\n&lt;p&gt;\n	请 相信我的承诺 虽然有点笨拙\n&lt;/p&gt;\n&lt;p&gt;\n	但我看见幸福的风\n&lt;/p&gt;\n&lt;p&gt;\n	如果我把我的手放在背后\n&lt;/p&gt;\n&lt;p&gt;\n	愿不愿意牵着一起走\n&lt;/p&gt;\n&lt;p&gt;\n	以为你只是一个美丽的偶然\n&lt;/p&gt;\n&lt;p&gt;\n	垂怜我不经意降落\n&lt;/p&gt;\n&lt;p&gt;\n	谁知道你不同 谁知道你不走\n&lt;/p&gt;\n&lt;p&gt;\n	拥抱着我说终于找到了我\n&lt;/p&gt;\n&lt;p&gt;\n	你 看穿我的冷漠 亲吻我的烦忧\n&lt;/p&gt;\n&lt;p&gt;\n	不在乎我曾经的错\n&lt;/p&gt;\n&lt;p&gt;\n	如果不是你的款款温柔\n&lt;/p&gt;\n&lt;p&gt;\n	还以为真爱 只是一个传说\n&lt;/p&gt;\n&lt;p&gt;\n	请 相信我的承诺 虽然有点笨拙\n&lt;/p&gt;\n&lt;p&gt;\n	但我看见幸福的风\n&lt;/p&gt;\n&lt;p&gt;\n	如果我把我的手放在背后\n&lt;/p&gt;\n&lt;p&gt;\n	愿不愿意牵着一起走\n&lt;/p&gt;\n&lt;p&gt;\n	你 看穿我的冷漠 亲吻我的烦忧\n&lt;/p&gt;\n&lt;p&gt;\n	不在乎我曾经的错\n&lt;/p&gt;\n&lt;p&gt;\n	如果不是你的款款温柔\n&lt;/p&gt;\n&lt;p&gt;\n	还以为真爱 只是一个传说\n&lt;/p&gt;\n&lt;p&gt;\n	请 相信我的承诺 虽然有点笨拙\n&lt;/p&gt;\n&lt;p&gt;\n	但我看见幸福的风\n&lt;/p&gt;\n&lt;p&gt;\n	如果我把我的手放在背后\n&lt;/p&gt;\n&lt;p&gt;\n	愿不愿意牵着一起走\n&lt;/p&gt;\n&lt;p&gt;\n	愿不愿意牵着到最后\n&lt;/p&gt;'),(8,8,'我们做的任何的决定，都没有错；但是，有可能会错过；错过你一辈子也找不回的曾今！');

/*Table structure for table `gm_addonvideo` */

DROP TABLE IF EXISTS `gm_addonvideo`;

CREATE TABLE `gm_addonvideo` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `aid` int(11) NOT NULL COMMENT '关联文章ID',
  `content` text COMMENT '内容',
  `video_url` text COMMENT '视频地址',
  PRIMARY KEY (`id`),
  KEY `aid` (`aid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `gm_addonvideo` */

insert  into `gm_addonvideo`(`id`,`aid`,`content`,`video_url`) values (1,2,'视频模型1','');

/*Table structure for table `gm_archive` */

DROP TABLE IF EXISTS `gm_archive`;

CREATE TABLE `gm_archive` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `typeid` int(11) NOT NULL COMMENT '所属分类',
  `mod` varchar(100) DEFAULT 'addonarticle' COMMENT '文章拓展表',
  `title` varchar(200) DEFAULT NULL COMMENT '标题',
  `title_color` varchar(50) DEFAULT NULL COMMENT '标题颜色',
  `title_weight` smallint(1) DEFAULT '0' COMMENT '标题加粗',
  `flag` set('a','h','c','s','j','p') DEFAULT NULL COMMENT '属性(头条[h]推荐[c]滚动[s]跳转[j])',
  `jumplink` varchar(200) DEFAULT NULL COMMENT '跳转地址',
  `litpic` varchar(200) DEFAULT NULL COMMENT '缩略图',
  `writer` int(11) DEFAULT NULL COMMENT '用户ID',
  `source` varchar(100) DEFAULT NULL COMMENT '来源',
  `keywords` varchar(200) DEFAULT NULL COMMENT '关键字',
  `description` text COMMENT '描述',
  `click` int(11) NOT NULL DEFAULT '0' COMMENT '点击数',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `typeid` (`typeid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `gm_archive` */

insert  into `gm_archive`(`id`,`typeid`,`mod`,`title`,`title_color`,`title_weight`,`flag`,`jumplink`,`litpic`,`writer`,`source`,`keywords`,`description`,`click`,`status`,`create_time`,`update_time`) values (1,6,'addonarticle','标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！','',0,'p','https://www.baidu.com/','/uploads/image/20170826/d7adf07b02ad855e948b032bb7720907.jpg',42,'','Guestbook','标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！标题很长，还有简介，还有图！！',19,1,1500698768,1504596689),(2,5,'addonarticle','只有标题','',0,'','','',42,'','','',6,1,1500997429,1504596694),(3,7,'addonarticle','只有简介，没有图','',0,'','','',42,'','','只有简介，没有图',61,1,1500997910,1504596694),(4,9,'addonarticle','你在等全世界的樱花，而我在樱花树下等你','',0,'p','','/uploads/image/20170826/a2f7e2021ed0494197a2dc7da220d93e.jpg',42,'','','',1,1,1504578094,1504596695),(5,9,'addonarticle','所谓缘分，就是遇见了该遇见的人','',0,'','','',42,'','','所谓缘分，就是遇见了该遇见的人',3,1,1504578161,1504596695),(6,9,'addonarticle','每一次遗憾都是对生命的一种肯定','',0,'','','',42,'','','每一次遗憾都是对生命的一种肯定',14,1,1504578196,1504596696),(7,8,'addonarticle','幸福的风','',0,'','','',42,'','','以为我永远只能一个人生活，孤单地快乐哀愁，偶尔可以伪装潇洒自由，心里慌了起来自己喝酒，以为你只是一个美丽的偶然，垂怜我不经意降落，谁知道你不同，谁知道你不走，拥抱着我说终于找到了我，你 看穿我的冷漠，亲吻我的烦忧，不在乎我曾经的错，如果不是你的款款温柔，还以为真爱只是一个传说。',29,1,1504578436,1569382655),(8,8,'addonarticle','我们做的任何的决定，都没有错；但是，有可能会错过；错过你一辈子也找不回的曾今','',0,'p','','/uploads/image/20170905/2c5f6ec66e1c707dec1e0c2887066b8a.jpg',42,'','','我们做的任何的决定，都没有错；但是，有可能会错过；错过你一辈子也找不回的曾今。\n希望我们的梦想，永远不会被忘记，希望有一天，我可以再见面！',147,1,1504595575,1504604570);

/*Table structure for table `gm_arctype` */

DROP TABLE IF EXISTS `gm_arctype`;

CREATE TABLE `gm_arctype` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) NOT NULL COMMENT '上级ID',
  `typename` varchar(100) NOT NULL COMMENT '栏目名称',
  `mid` int(11) NOT NULL DEFAULT '20' COMMENT '栏目模型ID',
  `target` varchar(50) NOT NULL COMMENT '弹出方式',
  `jumplink` varchar(200) NOT NULL COMMENT '外部跳转地址',
  `dirs` varchar(50) NOT NULL COMMENT '栏目目录',
  `litpic` varchar(200) DEFAULT NULL COMMENT '缩略图',
  `content` text COMMENT '内容',
  `sorts` int(11) NOT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `keywords` text COMMENT '关键字',
  `description` text COMMENT '描述',
  `templist` varchar(100) DEFAULT NULL COMMENT '列表页模板',
  `temparticle` varchar(100) DEFAULT NULL COMMENT '内容页模板',
  `pagesize` int(11) DEFAULT NULL COMMENT '分页条数',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `gm_arctype` */

insert  into `gm_arctype`(`id`,`pid`,`typename`,`mid`,`target`,`jumplink`,`dirs`,`litpic`,`content`,`sorts`,`status`,`keywords`,`description`,`templist`,`temparticle`,`pagesize`,`create_time`,`update_time`) values (1,0,'文档',21,'_self','','blog','','',1,1,'博客k','博客d','list_article','article_article',1,1500694046,1504685181),(2,0,'留言',20,'_self','','msg','','留言',2,1,'留言k','留言d','list_page','article_page',20,1500695122,1504599199),(3,0,'网站统计',20,'_self','','pie','','单页统计数据',3,1,'统计k','统计d','list_page','article_page',20,1500695130,1504687134),(4,0,'更新日志',20,'_self','','log','','&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;\n&lt;p&gt;\n	这里的更新日志慢慢完善！！！\n&lt;/p&gt;',4,1,'关于k','关于d','list_page','article_page',20,1500695142,1504687097),(5,1,'tp5',21,'_self','','tp5','','',1,1,'tp5k','tp5d','list_article','article_article',1,1500696489,1504576344),(6,1,'css',21,'_self','','css','','',2,1,'cssk','cssd','list_article','article_article',1,1500696800,1504576365),(7,1,'php',21,'_self','','php','','',3,1,'phpk','phpd','list_article','article_article',1,1500696912,1504576360),(8,1,'爱音乐',21,'_self','','music','','',8,1,'','','list_article','article_article',1,1504576437,1504576495),(9,1,'爱生活',21,'_self','','life','','',9,1,'','','list_article','article_article',1,1504576487,1504576487);

/*Table structure for table `gm_arctype_mod` */

DROP TABLE IF EXISTS `gm_arctype_mod`;

CREATE TABLE `gm_arctype_mod` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) DEFAULT NULL COMMENT '名称',
  `mod` varchar(100) DEFAULT NULL COMMENT '模型操作',
  `sorts` int(11) NOT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '50' COMMENT '是否显示',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

/*Data for the table `gm_arctype_mod` */

insert  into `gm_arctype_mod`(`id`,`name`,`mod`,`sorts`,`status`,`create_time`,`update_time`) values (20,'单页','addonpage',2,1,1494571907,1494572178),(21,'文章模型','addonarticle',1,1,1494571907,1494571930),(22,'外部链接','addonjump',999,1,1494571907,1494571932),(27,'相册模型','addonalbum',4,1,1494571907,1502787613),(28,'视频模型','addonvideo',3,1,1495004529,1502779823);

/*Table structure for table `gm_auth_group` */

DROP TABLE IF EXISTS `gm_auth_group`;

CREATE TABLE `gm_auth_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `module` varchar(10) NOT NULL DEFAULT 'admin' COMMENT '所属模块',
  `level` int(11) NOT NULL COMMENT '角色等级',
  `title` varchar(200) NOT NULL COMMENT '用户组中文名称',
  `status` tinyint(1) NOT NULL COMMENT '状态：为1正常，为0禁用',
  `rules` text COMMENT '用户组拥有的规则id， 多个规则","隔开',
  `notation` varchar(100) DEFAULT NULL COMMENT '组别描述',
  `pic` varchar(200) DEFAULT NULL COMMENT '角色图标',
  `recom` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否推荐首页显示',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gm_auth_group` */

insert  into `gm_auth_group`(`id`,`module`,`level`,`title`,`status`,`rules`,`notation`,`pic`,`recom`,`create_time`,`update_time`) values (1,'admin',1090,'超级管理员',1,'1,126,127,135,136,137,128,138,139,140,129,141,142,143,130,144,145,146,131,147,148,149,132,133,150,151,152,134,153,154,155,116,117,122,118,125,119,124,50,57,58,59,60,61,62,63,64,51,52,53,54,2,3,29,30,31,56,4,32,33,34,55,5,11,12,13,6,14,27,28,70,94,95,96,97,71,72,73,39,40,41,42,43,46,44,45,47,65,105,106,107,108,109,110,111,112,113,48,49,92,93,80,81,82,83,84,85,86,87,88,89,90,91,98,99,100,101,102,103,104','我能干任何事','#dd4b39',0,1502780231,1570867738),(2,'admin',1,'后台浏览',1,'1,50,57,61,51,2,3,4,5,6,70,94,71,39,44,47,65,48,49,92,80,84,88,98,99,101','只能查看列表','#f39c12',0,1502784113,1502852075);

/*Table structure for table `gm_auth_group_access` */

DROP TABLE IF EXISTS `gm_auth_group_access`;

CREATE TABLE `gm_auth_group_access` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) unsigned NOT NULL COMMENT '用户id',
  `group_id` int(11) unsigned NOT NULL COMMENT '用户组id',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE,
  KEY `group_id` (`group_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gm_auth_group_access` */

insert  into `gm_auth_group_access`(`id`,`uid`,`group_id`,`create_time`,`update_time`) values (1,1,1,1502782413,1502782413),(2,2,2,1502784196,1502784196);

/*Table structure for table `gm_auth_rule` */

DROP TABLE IF EXISTS `gm_auth_rule`;

CREATE TABLE `gm_auth_rule` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pid` int(11) unsigned NOT NULL COMMENT '父id',
  `module` varchar(10) NOT NULL DEFAULT 'admin' COMMENT '权限节点所属模块',
  `level` tinyint(1) NOT NULL COMMENT '1-项目;2-模块;3-操作',
  `name` varchar(80) NOT NULL COMMENT '规则唯一标识',
  `title` varchar(20) NOT NULL COMMENT '规则中文名称',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `ismenu` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否导航',
  `condition` varchar(200) DEFAULT NULL COMMENT '规则表达式，为空表示存在就验证，不为空表示按照条件验证',
  `icon` varchar(50) DEFAULT NULL COMMENT '节点图标',
  `sorts` mediumint(8) DEFAULT '50' COMMENT '排序',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`) USING BTREE,
  KEY `module` (`module`) USING BTREE,
  KEY `level` (`level`) USING BTREE,
  KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

/*Data for the table `gm_auth_rule` */

insert  into `gm_auth_rule`(`id`,`pid`,`module`,`level`,`name`,`title`,`type`,`status`,`ismenu`,`condition`,`icon`,`sorts`,`create_time`,`update_time`) values (1,0,'admin',1,'Index/index','后台首页',1,1,1,'','fa fa-home',1010,0,1502786208),(2,0,'admin',1,'leftUser','用户管理',1,1,1,'','fa fa-users',1050,0,1497169240),(3,2,'admin',2,'User/index','用户列表',1,1,1,'','fa fa-user-o',1,0,1493808384),(4,2,'admin',2,'AuthGroup/index','角色管理',1,1,1,'','fa fa-vcard',2,0,1493808400),(5,2,'admin',2,'AuthRule/index','管理员节点列表',1,1,1,'','fa fa-user-circle',3,0,1493808416),(6,2,'admin',2,'AuthRuleM/index','会员节点列表',1,1,1,'','fa fa-user-circle-o',4,0,1494058614),(11,5,'admin',3,'AuthRule/create','新增管理员节点',1,1,0,'','',1,0,0),(12,5,'admin',3,'AuthRule/edit','编辑管理员节点',1,1,0,'','',2,0,0),(13,5,'admin',3,'AuthRule/delete','删除管理员节点',1,1,0,'','',3,0,0),(14,6,'admin',3,'AuthRuleM/create','新增会员节点',1,1,0,'','',1,0,1494059133),(27,6,'admin',3,'AuthRuleM/edit','编辑会员节点',1,1,0,'','',2,0,1494058640),(28,6,'admin',3,'AuthRuleM/delete','删除会员节点',1,1,0,'','',3,0,1494058655),(29,3,'admin',3,'User/create','新增用户',1,1,0,'','',1,0,0),(30,3,'admin',3,'User/edit','编辑用户',1,1,0,'','',2,0,0),(31,3,'admin',3,'User/delete','删除用户',1,1,0,'','',3,0,0),(32,4,'admin',3,'AuthGroup/create','新增角色',1,1,0,'','',1,0,0),(33,4,'admin',3,'AuthGroup/edit','编辑角色',1,1,0,'','',2,0,0),(34,4,'admin',3,'AuthGroup/delete','删除角色',1,1,0,'','',3,0,0),(35,0,'member',1,'Member','会员中心',1,1,1,'','',2000,0,1499163921),(36,35,'member',2,'Member/index','个人资料',1,0,1,'','',1,0,1499163690),(39,0,'admin',1,'leftSystem','系统管理',1,1,1,'','fa fa-gear',1070,1493809020,1497169226),(40,39,'admin',2,'Config/index','系统配置字段',1,1,1,'','fa fa-gears',1,1493809348,1493809384),(41,40,'admin',3,'Config/create','新增字段',1,1,0,'','',1,1493809465,1493809465),(42,40,'admin',3,'Config/edit','编辑字段',1,1,0,'','',2,1493809511,1493809511),(43,40,'admin',3,'Config/delete','删除字段',1,1,0,'','',3,1493809555,1493809555),(44,39,'admin',2,'Config/web','站点配置',1,1,1,'','fa fa-desktop',2,1493809933,1494439144),(45,39,'admin',2,'Config/system','系统配置',1,1,1,'','fa fa-dot-circle-o',3,1493810207,1494439156),(46,40,'admin',3,'Config/save','保存字段',1,1,0,'','',4,1494066111,1494066119),(47,39,'admin',2,'Config/up','上传配置',1,1,1,'','fa fa-upload',4,1494067075,1494439160),(48,0,'admin',1,'leftTool','站长工具',1,1,1,'','fa fa-wrench',1080,1494432073,1501768515),(49,48,'admin',2,'Index/cleanCache','清除缓存',1,1,1,'','fa fa-trash-o',1,1494433222,1494433701),(50,0,'admin',1,'leftInfo','文章管理',1,1,1,'','fa fa-file-text',1030,1494556641,1497169249),(51,50,'admin',2,'ArctypeMod/index','文章模型管理',1,1,1,'','fa fa-list-alt',99,1494559590,1494559959),(52,51,'admin',3,'ArctypeMod/create','新增文章模型',1,1,0,'','',1,1494559700,1494559700),(53,51,'admin',3,'ArctypeMod/edit','编辑文章模型',1,1,0,'','',2,1494559855,1494559855),(54,51,'admin',3,'ArctypeMod/delete','删除文章模型',1,1,0,'','',3,1494560030,1494560030),(55,4,'admin',3,'AuthGroup/authUser','授权用户',1,1,0,'','',4,1494585527,1494585527),(56,3,'admin',3,'User/authGroup','授权角色',1,1,0,'','',4,1494660839,1494660887),(57,50,'admin',2,'Arctype/index','文章分类',1,1,1,'','fa fa-columns',1,1494920741,1494986230),(58,57,'admin',3,'Arctype/create','新增文章分类',1,1,0,'','',1,1494921065,1501768850),(59,57,'admin',3,'Arctype/edit','编辑文章分类',1,1,0,'','',2,1494921102,1494986239),(60,57,'admin',3,'Arctype/delete','删除文章分类',1,1,0,'','',3,1494921185,1494986243),(61,50,'admin',2,'Archive/index','文章列表',1,1,1,'','fa fa-file-text-o',2,1495000296,1495000391),(62,61,'admin',3,'Archive/create','新增文章',1,1,0,'','',1,1495000482,1499080778),(63,61,'admin',3,'Archive/edit','编辑文章',1,1,0,'','',2,1495000513,1499080779),(64,61,'admin',3,'Archive/delete','删除文章',1,1,0,'','',3,1495000551,1499080779),(65,39,'admin',2,'Config/sms','短信配置',1,1,1,'','fa fa-envelope-square',5,1496207264,1496208848),(70,0,'admin',1,'leftMessage','消息管理',1,1,1,'','fa fa-comments',1062,1497169625,1501655677),(71,70,'admin',2,'Comment/index','评论列表',1,1,1,'','fa fa-comments-o',2,1497169866,1501662981),(72,71,'admin',3,'Comment/edit','编辑评论',1,1,0,'','',1,1497170140,1497170140),(73,71,'admin',3,'Comment/delete','删除评论',1,1,0,'','',2,1497170195,1497170195),(80,48,'admin',2,'ModuleClass/index','模块分类',1,1,1,'','fa fa-modx',4,1499307912,1501574486),(81,80,'admin',3,'ModuleClass/create','新增模块',1,1,0,'','',1,1499308130,1499308130),(82,80,'admin',3,'ModuleClass/edit','编辑模块',1,1,0,'','',2,1499308201,1499308201),(83,80,'admin',3,'ModuleClass/delete','删除模块',1,1,0,'','',3,1499308233,1499308233),(84,48,'admin',2,'Flink/index','友情链接',1,1,1,'','fa fa-link',5,1499308667,1501574477),(85,84,'admin',3,'Flink/create','新增友情链接',1,1,0,'','',1,1499308712,1499308712),(86,84,'admin',3,'Flink/edit','编辑友情链接',1,1,0,'','',2,1499308748,1499308748),(87,84,'admin',3,'Flink/delete','删除友情链接',1,1,0,'','',3,1499308846,1499308846),(88,48,'admin',2,'Banner/index','BANNER管理',1,1,1,'','fa fa-object-group',6,1499308946,1502853250),(89,88,'admin',3,'Banner/create','新增banner',1,1,0,'','',1,1499308966,1502853277),(90,88,'admin',3,'Banner/edit','编辑banner',1,1,0,'','',2,1499309075,1502853278),(91,88,'admin',3,'Banner/delete','删除banner',1,1,0,'','',3,1499309099,1502853279),(92,48,'admin',2,'LoginLog/index','登录日志',1,1,1,'','fa fa-location-arrow',2,1501574804,1501574804),(93,92,'admin',3,'LoginLog/delete','删除登录日志',1,1,0,'','',1,1501575100,1501575100),(94,70,'admin',2,'Guestbook/index','留言列表',1,1,1,'','fa fa-comment-o',1,1501660017,1501662972),(95,94,'admin',3,'Guestbook/create','新增留言',1,1,0,'','',1,1501660085,1501660085),(96,94,'admin',3,'Guestbook/edit','编辑留言',1,1,0,'','',2,1501660119,1501660119),(97,94,'admin',3,'Guestbook/delete','删除留言',1,1,0,'','',3,1501660170,1501660170),(98,0,'admin',1,'leftDatabase','数据库管理',1,1,1,'','fa fa-database',1090,1501771385,1501771460),(99,98,'admin',2,'Database/index','数据库列表',1,1,1,'','fa fa-list',1,1501771697,1501771697),(100,99,'admin',3,'Database/backup','备份数据库',1,1,0,'','',1,1502417821,1502418076),(101,98,'admin',2,'Database/reduction','还原数据库',1,1,1,'','fa fa-rotate-left',2,1502418268,1502418268),(102,101,'admin',3,'Database/restore','还原',1,1,0,'','',1,1502419840,1502419840),(103,101,'admin',3,'Database/dowonload','下载',1,1,0,'','',2,1502419878,1502419878),(104,101,'admin',3,'Database/delete','删除',1,1,0,'','',3,1502419916,1502419916),(105,0,'admin',1,'leftApi','API接口管理',1,1,1,'','fa fa-magic',1071,1509205539,1509205539),(106,105,'admin',2,'TokenApi/index','API接口列表',1,1,1,'','fa fa-list-ul',1,1509205595,1509205595),(107,106,'admin',3,'TokenApi/create','新增API接口',1,1,0,'','',1,1509205654,1509205654),(108,106,'admin',3,'TokenApi/edit','编辑API接口',1,1,0,'','',2,1509205697,1509205697),(109,106,'admin',3,'TokenApi/delete','删除API接口',1,1,0,'','',3,1509205722,1509205722),(110,106,'admin',3,'TokenApi/token','生成Token',1,1,0,'','',4,1509205748,1509205748),(111,106,'admin',3,'TokenApi/generateDocument','生成文档',1,1,0,'','',5,1509205781,1509205781),(112,106,'admin',3,'TokenApi/viewDocument','查看文档',1,1,0,'','',6,1509205816,1509205816),(113,105,'admin',2,'Config/api','接口配置',1,1,1,'','fa fa-gears',2,1509205856,1509205856),(116,0,'admin',1,'666','陪玩管理',1,1,1,NULL,'fa fa-list-alt',1020,1569309170,1569842362),(117,116,'admin',2,'Pwfriend/index','好友管理列表',1,1,1,NULL,'',51,1569309341,1569381370),(118,116,'admin',2,'Pwhistory/index','历史管理列表',1,1,1,NULL,'',52,1569309658,1569397764),(119,116,'admin',2,'Pwcollection/index','收藏管理列表',1,1,1,NULL,'',55,1569309717,1569394497),(122,117,'admin',3,'Pwfriend/delete','删除好友',1,1,0,NULL,'',58,1569380073,1569397822),(124,119,'admin',3,'Pwcollection/delete','删除记录',1,1,0,NULL,'',50,1569393835,1569397825),(125,118,'admin',3,'Pwhistory/delete','删除历史',1,1,0,NULL,'',50,1569397794,1569397824),(126,0,'admin',1,'Ys','影视管理',1,1,1,NULL,'fa fa-desktop',1011,1570846367,1570846429),(127,126,'admin',2,'Ysdaohang/index','导航管理',1,1,1,NULL,'',50,1570848457,1570848457),(128,126,'admin',2,'Ysbanner/index','banner图管理',1,1,1,NULL,'',50,1570848490,1570848490),(129,126,'admin',2,'Yscoop/index','合作伙伴管理',1,1,1,NULL,'',50,1570848522,1570848522),(130,126,'admin',2,'Ysmovie/index','电影管理',1,1,1,NULL,'',50,1570848546,1570848546),(131,126,'admin',2,'Ystv/index','电视剧管理',1,1,1,NULL,'',50,1570848567,1570848567),(133,126,'admin',2,'Ysjingdian/index','经典影视管理',1,1,1,NULL,'',50,1570848623,1570848633),(134,126,'admin',2,'Ysnews/index','新闻资讯管理',1,1,1,NULL,'',50,1570848672,1570848672),(135,127,'admin',3,'Ysdaohang/create','添加导航',1,1,0,NULL,'',50,1570849972,1570861277),(136,127,'admin',3,'Ysdaohang/edit','修改导航',1,1,0,NULL,'',50,1570849995,1570861278),(137,127,'admin',3,'Ysdaohang/delete','删除导航',1,1,0,NULL,'',50,1570850011,1570861278),(138,128,'admin',3,'Ysbanner/create','添加banner图',1,1,0,NULL,'',50,1570850952,1570861281),(139,128,'admin',3,'Ysbanner/edit','修改banner图',1,1,0,NULL,'',50,1570850975,1570861284),(140,128,'admin',3,'Ysbanner/delete','删除banner图',1,1,0,NULL,'',50,1570851014,1570861284),(141,129,'admin',3,'Yscoop/create','添加合作伙伴',1,1,0,NULL,'',50,1570852340,1570861287),(142,129,'admin',3,'Yscoop/edit','修改合作伙伴',1,1,0,NULL,'',50,1570852400,1570861288),(143,129,'admin',3,'Yscoop/delete','删除合作伙伴',1,1,0,NULL,'',50,1570852430,1570861289),(144,130,'admin',3,'Ysmovie/create','添加电影',1,1,0,NULL,'',50,1570861253,1570861253),(145,130,'admin',3,'Ysmovie/edit','修改电影',1,1,0,NULL,'',50,1570861330,1570861330),(146,130,'admin',3,'Ysmovie/delete','删除电影',1,1,0,NULL,'',50,1570861348,1570861348),(147,131,'admin',3,'Ystv/create','添加电视剧',1,1,0,NULL,'',50,1570863959,1570863959),(148,131,'admin',3,'Ystv/edit','修改电视剧',1,1,0,NULL,'',50,1570863978,1570863978),(149,131,'admin',3,'Ystv/delete','删除电视剧',1,1,0,NULL,'',50,1570864000,1570864000),(150,133,'admin',3,'Ysjingdian/create','添加经典',1,1,0,NULL,'',50,1570864698,1570864698),(151,133,'admin',3,'Ysjingdian/edit','修改经典',1,1,0,NULL,'',50,1570864720,1570864720),(152,133,'admin',3,'Ysjingdian/delete','删除经典',1,1,0,NULL,'',50,1570864738,1570864738),(153,134,'admin',3,'Ysnews/create','添加新闻',1,1,0,NULL,'',50,1570867671,1570867671),(154,134,'admin',3,'Ysnews/edit','修改新闻',1,1,0,NULL,'',50,1570867699,1570867699),(155,134,'admin',3,'Ysnews/delete','删除新闻',1,1,0,NULL,'',50,1570867726,1570867726);

/*Table structure for table `gm_banner` */

DROP TABLE IF EXISTS `gm_banner`;

CREATE TABLE `gm_banner` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mid` int(11) NOT NULL COMMENT '模块分类ID',
  `title` varchar(250) NOT NULL COMMENT '标题',
  `url` varchar(250) DEFAULT NULL COMMENT '跳转链接',
  `litpic` varchar(250) DEFAULT NULL COMMENT '图片',
  `info` text COMMENT '简介说明',
  `sorts` int(11) NOT NULL COMMENT '排序',
  `status` int(11) NOT NULL COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gm_banner` */

/*Table structure for table `gm_collection` */

DROP TABLE IF EXISTS `gm_collection`;

CREATE TABLE `gm_collection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) DEFAULT NULL,
  `sname` varchar(10) DEFAULT NULL,
  `works` varchar(100) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `utime` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gm_collection` */

insert  into `gm_collection`(`id`,`name`,`sname`,`works`,`ctime`,`utime`) values (1,'王晓亮','好热',NULL,1569571266,NULL);

/*Table structure for table `gm_comment` */

DROP TABLE IF EXISTS `gm_comment`;

CREATE TABLE `gm_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mid` int(11) NOT NULL COMMENT '被评论用户id',
  `uid` int(11) NOT NULL COMMENT '评论用户id',
  `content` text COMMENT '评论内容',
  `recontent` text COMMENT '回复内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`) USING BTREE,
  KEY `uid` (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `gm_comment` */

/*Table structure for table `gm_config` */

DROP TABLE IF EXISTS `gm_config`;

CREATE TABLE `gm_config` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `k` char(50) DEFAULT NULL COMMENT '键',
  `v` text COMMENT '值',
  `type` varchar(50) DEFAULT NULL COMMENT '类型',
  `desc` text COMMENT '描述',
  `prompt` varchar(250) DEFAULT NULL COMMENT '提示',
  `sorts` int(11) DEFAULT NULL COMMENT '排序',
  `status` tinyint(1) NOT NULL COMMENT '是否显示',
  `texttype` varchar(100) DEFAULT NULL COMMENT '文本类型',
  `textvalue` varchar(100) DEFAULT NULL COMMENT '文本选项值',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `k` (`k`) USING BTREE,
  KEY `type` (`type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Data for the table `gm_config` */

insert  into `gm_config`(`id`,`k`,`v`,`type`,`desc`,`prompt`,`sorts`,`status`,`texttype`,`textvalue`,`create_time`,`update_time`) values (1,'title','苏晓信','web','网站标题','网站标题【title】',1,1,'text','',1493863845,1501663526),(2,'logo','/uploads/image/20170826/a2f7e2021ed0494197a2dc7da220d93e.jpg','web','网站LOGO','网站LOGO，一般用于导航或底部的LOGO图片',2,1,'image','',1493864083,1501663570),(3,'person','苏晓信','web','联系人','联系人',3,1,'text','',1493864150,1493864480),(4,'address','184278846','web','联系地址','联系地址',4,1,'text','',1493864266,1493864485),(5,'keywords','苏晓信-keywords','web','网站关键字','网站关键字，网站首页的【keywords】',5,1,'text','',1493864340,1501663645),(6,'description','苏晓信-description','web','网站描述','网站描述，网站首页的【description】',6,1,'textarea','',1493864454,1501663673),(7,'copyright','Copyright © 2017-2018 苏晓信','web','网站备案号','网站备案号',7,1,'text','',1493864547,1493864568),(8,'isbrowse','0','system','系统浏览模式','系统浏览模式',1,0,'select','',1494066335,1505468914),(9,'islog','0','system','是否开启系统日志','是否开启系统日志',2,0,'select','',1494066832,1505468915),(10,'image_format','jpg,gif,jpeg,png,bmp','up','上传图片格式','上传图片格式',1,1,'text','',1494067463,1499080988),(11,'image_size','5242880','up','上传文件大小','1024：1KB，1048576：1MB，5242880：5MB。建议不要超过5MB，避免文件上传失败',5,1,'text','',1494067564,1501572699),(12,'image_print','/static/global/face/logo.png','up','水印图片','水印图片，可为上传的图片添加水印，开启了图片水印功能，请必须上传水印图片',8,1,'image','',1494067681,1501664409),(13,'file_format','doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar','up','上传文件格式','上传文件格式',2,1,'text','',1495940879,1495941568),(14,'flash_format','swf,flv','up','上传Flash格式','上传Flash格式',3,1,'text','',1495940963,1495941577),(15,'media_format','swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb','up','上传视音频格式','上传视音频格式',4,1,'text','',1495941005,1495941582),(16,'upload_path','uploads','up','文件上传目录','文件上传目录',6,1,'text','',1495942885,1495943186),(17,'isprint','1','up','是否开启图片水印','是否开启图片水印',7,1,'select','',1495943270,1495943372),(18,'print_position','9','up','水印图片位置','水印图片位置',9,1,'select','',1495996448,1495996448),(19,'print_blur','70','up','水印图片透明度','水印图片透明度，取值范围【0-100】',10,1,'text','',1495996522,1501664253),(20,'appkey','【阿里大于】短信appkey','sms','短信appkey','短信appkey',1,1,'text','',1496207509,1499163943),(21,'secretKey','【阿里大于】短信secretKey','sms','短信secretKey','短信secretKey',2,1,'text','',1496207647,1499080992),(22,'sms_free_sign_name','【阿里大于】短信签名','sms','短信签名','短信签名',3,1,'text','',1496208387,1499080992),(23,'sms_code_tpl_code','【阿里大于】SMS_00000001','sms','短信注册模板','短信注册模板',5,1,'text','',1496208437,1499080991),(24,'sms_pwd_tpl_code','【阿里大于】SMS_00000002','sms','短信密码找回模板','短信密码找回模板',6,1,'text','',1496208571,1500478651),(25,'image_url','','up','图片上传域名地址','图片上传域名地址，图片路径保存数据库是否带域名，不建议填写，除非很清楚怎么使用',11,1,'text','',1496295604,1501664181),(26,'sms_end_time','【阿里大于】30','sms','短信验证时限','短信验证时单位：分，只填整数',4,1,'text','',1498101884,1500478650),(27,'login_time','6000','system','登陆超时时限','登陆系统多久时间不操作，重新登陆系统，数字整数【10:10秒】',3,1,'text','',1505468873,1505468873),(28,'api_token_encryption','sxx','api','token加密','API接口token接口令牌加密配置字段',1,1,'text','',1509206015,1509206015),(29,'api_url','http://dq.base.com','api','接口地址','API接口外部访问地址，可为IP地址',2,1,'text','',1509206051,1509206064);

/*Table structure for table `gm_flink` */

DROP TABLE IF EXISTS `gm_flink`;

CREATE TABLE `gm_flink` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `mid` int(11) NOT NULL COMMENT '模块分类ID',
  `webname` varchar(250) NOT NULL COMMENT '网站名称',
  `url` varchar(250) DEFAULT NULL COMMENT '网站链接',
  `info` text COMMENT '网站简介',
  `email` varchar(250) DEFAULT NULL COMMENT '站长邮箱',
  `logo` varchar(250) DEFAULT NULL COMMENT '网站logo地址',
  `sorts` int(11) NOT NULL COMMENT '排序',
  `status` int(11) NOT NULL COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gm_flink` */

insert  into `gm_flink`(`id`,`mid`,`webname`,`url`,`info`,`email`,`logo`,`sorts`,`status`,`create_time`,`update_time`) values (1,1,'百度','https://www.baidu.com/','百度网址','1234@qq.com','https://ss0.bdstatic.com/5aV1bjqh_Q23odCf/static/superman/img/logo/bd_logo1_31bdc765.png',1,1,1509244824,1509245150),(2,1,'新浪微博','http://weibo.com/','','123@qq.com','',1,1,1509244966,1509244966);

/*Table structure for table `gm_friend` */

DROP TABLE IF EXISTS `gm_friend`;

CREATE TABLE `gm_friend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL COMMENT '连接陪玩信息',
  `fname` int(11) NOT NULL COMMENT '连接登陆人',
  `ctime` int(11) NOT NULL COMMENT '创建时间',
  `utime` int(11) NOT NULL COMMENT '修改时间',
  `type` tinyint(2) NOT NULL COMMENT '1.收藏 2.关注',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='历史纪录';

/*Data for the table `gm_friend` */

insert  into `gm_friend`(`id`,`name`,`fname`,`ctime`,`utime`,`type`) values (1,1,2,1569571266,0,1);

/*Table structure for table `gm_guestbook` */

DROP TABLE IF EXISTS `gm_guestbook`;

CREATE TABLE `gm_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `title` varchar(250) DEFAULT NULL COMMENT '标题',
  `email` varchar(250) DEFAULT NULL COMMENT '邮箱',
  `content` text COMMENT '评论内容',
  `recontent` text COMMENT '回复内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `gm_guestbook` */

insert  into `gm_guestbook`(`id`,`uid`,`title`,`email`,`content`,`recontent`,`status`,`create_time`,`update_time`) values (1,1,'hello','','欢迎您的反馈','',1,1502786251,1502786251);

/*Table structure for table `gm_history` */

DROP TABLE IF EXISTS `gm_history`;

CREATE TABLE `gm_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` int(11) NOT NULL,
  `hname` int(11) NOT NULL,
  `ctime` int(11) NOT NULL,
  `utime` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `gm_history` */

insert  into `gm_history`(`id`,`name`,`hname`,`ctime`,`utime`) values (1,2,3,1569571266,0);

/*Table structure for table `gm_login_log` */

DROP TABLE IF EXISTS `gm_login_log`;

CREATE TABLE `gm_login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL COMMENT '登陆用户ID',
  `ip` varchar(100) NOT NULL COMMENT 'IP地址',
  `country` varchar(100) DEFAULT NULL COMMENT '国家',
  `province` varchar(100) DEFAULT NULL COMMENT '省',
  `city` varchar(100) DEFAULT NULL COMMENT '城市',
  `district` varchar(100) DEFAULT NULL COMMENT '区',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

/*Data for the table `gm_login_log` */

insert  into `gm_login_log`(`id`,`uid`,`ip`,`country`,`province`,`city`,`district`,`status`,`create_time`,`update_time`) values (1,1,'220.166.96.3','中国','四川','泸州','',1,1502782475,1502782475),(2,1,'106.92.245.226','中国','重庆','重庆','',1,1502782523,1502782523),(3,2,'106.92.245.226','中国','重庆','重庆','',1,1502784480,1502784480),(4,1,'106.92.245.226','中国','重庆','重庆','',1,1502784517,1502784517),(5,1,'106.92.245.226','中国','重庆','重庆','',1,1502785104,1502785104),(6,2,'106.92.245.226','中国','重庆','重庆','',1,1502785133,1502785133),(7,1,'106.92.245.226','中国','重庆','重庆','',1,1502785156,1502785156),(8,1,'106.92.245.226','中国','重庆','重庆','',1,1502785177,1502785177),(9,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569293925,1569293925),(10,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569305541,1569305541),(11,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569310870,1569310870),(12,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569324105,1569324105),(13,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569327187,1569327187),(14,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569330369,1569330369),(15,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569330562,1569330562),(16,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569370122,1569370122),(17,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569377016,1569377016),(18,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569391604,1569391604),(19,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569455294,1569455294),(20,1,'127.0.0.1',NULL,NULL,NULL,NULL,1,1569464068,1569464068);

/*Table structure for table `gm_module_class` */

DROP TABLE IF EXISTS `gm_module_class`;

CREATE TABLE `gm_module_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `title` varchar(250) NOT NULL COMMENT '标题',
  `action` varchar(250) NOT NULL COMMENT '模块操作',
  `sorts` int(11) NOT NULL COMMENT '排序',
  `status` int(11) NOT NULL COMMENT '状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `gm_module_class` */

insert  into `gm_module_class`(`id`,`title`,`action`,`sorts`,`status`,`create_time`,`update_time`) values (1,'友情链接','flink',1001,1,1499310094,1500693625),(2,'合作伙伴','flink',1002,1,1499310095,1502778579),(3,'首页顶部banner','banner',2001,1,1499310182,1499310326),(4,'内页banner','banner',2002,1,1499310261,1499310326);

/*Table structure for table `gm_token_api` */

DROP TABLE IF EXISTS `gm_token_api`;

CREATE TABLE `gm_token_api` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `name` varchar(250) NOT NULL COMMENT '接口名称',
  `module` varchar(250) NOT NULL COMMENT '模块',
  `controller` varchar(250) NOT NULL COMMENT '控制器',
  `method` varchar(250) NOT NULL COMMENT '方法',
  `param` varchar(250) DEFAULT NULL COMMENT '参数',
  `is_user_token` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否验证用户token',
  `is_api_token` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否验证接口token',
  `type` varchar(50) NOT NULL DEFAULT 'GET' COMMENT '请求方式',
  `token` char(32) DEFAULT NULL COMMENT '接口令牌',
  `document` text COMMENT '接口文档',
  `remark` text NOT NULL COMMENT '接口备注',
  `sorts` int(11) NOT NULL DEFAULT '50' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态：为1正常，为0禁用',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `gm_token_api` */

insert  into `gm_token_api`(`id`,`name`,`module`,`controller`,`method`,`param`,`is_user_token`,`is_api_token`,`type`,`token`,`document`,`remark`,`sorts`,`status`,`create_time`,`update_time`) values (1,'友情链接列表','api','Flink','index','',0,0,'GET','0ab50bda0a40bf078a415deab654eb19','{\"url\":\"/api/flink\"}','?orderBy[]=id,desc&amp;page=1&amp;pageSize=2\n&amp;addon=moduleClass 【关联模块分类表】\n当前接口未开启任何token验证，可直接使用',1,1,1505660032,1509245191),(2,'友情链接单条查询','api','Flink','index','id',1,0,'GET','3708626bc9c864bc88465f4ffe79919c','{\"url\":\"/api/flink/1\"}','&amp;addon=moduleClass 【关联模块分类表】\n当前接口开启用户token，仅开启ID为1的admin用户token【61e5146e54db6be2d7174530417ea40b】\n?token_uid=1&amp;token_user=61e5146e54db6be2d7174530417ea40b',2,1,1505660082,1509244598),(3,'友情链接新增','api','Flink','index','',0,1,'POST','4aefb101ea274fda9a62e234b56714df','{\"url\":\"/api/flink\",\"data\":[{\"key\":\"mid\",\"value\":\"1\",\"type\":\"text\",\"enabled\":true},{\"key\":\"webname\",\"value\":\"\",\"type\":\"text\",\"enabled\":true},{\"key\":\"url\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"info\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"email\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"logo\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"sorts\",\"value\":\"50\",\"type\":\"text\",\"enabled\":true},{\"key\":\"status\",\"value\":\"1\",\"type\":\"text\",\"enabled\":true}]}','当前接口开启接口token验证\n?token_api=4aefb101ea274fda9a62e234b56714df【接口令牌token】',3,1,1507274504,1509171720),(4,'友情链接编辑','api','Flink','index','id',1,1,'PUT','656e9258bbcdebf31271317681e9a3a0','{\"url\":\"/api/flink/1\",\"data\":[{\"key\":\"mid\",\"value\":\"1\",\"type\":\"text\",\"enabled\":true},{\"key\":\"webname\",\"value\":\"\",\"type\":\"text\",\"enabled\":true},{\"key\":\"url\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"info\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"email\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"logo\",\"value\":\"\",\"type\":\"text\",\"enabled\":false},{\"key\":\"sorts\",\"value\":\"50\",\"type\":\"text\",\"enabled\":true},{\"key\":\"status\",\"value\":\"1\",\"type\":\"text\",\"enabled\":true}]}','当前接口开启用户token和接口token验证，仅开启ID为1的admin用户token【61e5146e54db6be2d7174530417ea40b】\n&amp;token_uid=1&amp;token_user=61e5146e54db6be2d7174530417ea40b\n&amp;token_api=656e9258bbcdebf31271317681e9a3a0【接口令牌token】',4,1,1507274570,1509244689),(5,'友情链接删除','api','Flink','index','id',0,0,'DELETE','6fdb7f3a2ea04eb4ca746e64ecd96161','{\"url\":\"/api/flink/6\"}','删除需要数据库成功删除才会返回200\n当前接口未开启任何token验证，可直接使用',5,0,1507278825,1509169242),(6,'友情链接自定义','api','Flink','demo','',0,0,'GET','75726358b96d9ca2b6c760bb01a4df09','{\"url\":\"/api/flink/demo\"}','1',6,1,1509163375,1509163924);

/*Table structure for table `gm_token_user` */

DROP TABLE IF EXISTS `gm_token_user`;

CREATE TABLE `gm_token_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型【1、PC，2、移动端】',
  `token` char(32) NOT NULL COMMENT 'token令牌',
  `token_time` int(10) DEFAULT NULL COMMENT 'token令牌时限【PC登陆超时】',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `gm_token_user` */

insert  into `gm_token_user`(`id`,`uid`,`type`,`token`,`token_time`,`create_time`,`update_time`) values (10,1,1,'4b9ecbac411325c7ca51556fc1ba66c8',1571115428,1505569684,1505569684),(11,2,1,'e418db053f856dabc798ab5cf9323a34',1569511255,1569505159,1569505159);

/*Table structure for table `gm_user` */

DROP TABLE IF EXISTS `gm_user`;

CREATE TABLE `gm_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `name` varchar(100) DEFAULT NULL COMMENT '姓名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `moblie` varchar(50) DEFAULT NULL COMMENT '手机号码',
  `sex` tinyint(1) NOT NULL DEFAULT '0' COMMENT '性别',
  `logins` int(11) NOT NULL DEFAULT '0' COMMENT '登陆次数',
  `create_time` int(10) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `update_time` int(10) NOT NULL DEFAULT '0' COMMENT '更新时间',
  `reg_ip` varchar(200) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `last_time` int(10) NOT NULL DEFAULT '0' COMMENT '最后登录时间',
  `last_ip` varchar(200) NOT NULL DEFAULT '0' COMMENT '最后登录IP',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `gm_user` */

insert  into `gm_user`(`id`,`username`,`password`,`name`,`email`,`moblie`,`sex`,`logins`,`create_time`,`update_time`,`reg_ip`,`last_time`,`last_ip`,`status`) values (1,'admin','e10adc3949ba59abbe56e057f20f883e','王晓亮','2992088140@qq.com','13256519593',1,69,1502781914,1569295079,'127.0.0.1',1571109399,'127.0.0.1',1),(2,'test1','e10adc3949ba59abbe56e057f20f883e','测试账号','','',1,6,1502782875,1502783821,'127.0.0.1',1569505159,'127.0.0.1',1),(3,'不高兴','e10adc3949ba59abbe56e057f20f883e','不高兴','','',1,0,1502785283,1502785283,'127.0.0.1',1502785283,'127.0.0.1',1),(4,'没头脑','e10adc3949ba59abbe56e057f20f883e','没头脑','','',1,0,1502785316,1502785316,'127.0.0.1',1502785316,'127.0.0.1',1),(5,'么么么么','e10adc3949ba59abbe56e057f20f883e','小媳妇','','',1,0,1502785706,1569331272,'127.0.0.1',1502785706,'127.0.0.1',1);

/*Table structure for table `gm_user_info` */

DROP TABLE IF EXISTS `gm_user_info`;

CREATE TABLE `gm_user_info` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `uid` int(11) NOT NULL COMMENT '用户ID',
  `avatar` varchar(200) DEFAULT NULL COMMENT '头像',
  `qq` varchar(100) DEFAULT NULL COMMENT 'QQ',
  `birthday` varchar(100) DEFAULT NULL COMMENT '生日',
  `info` text COMMENT '用户信息',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '编辑时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Data for the table `gm_user_info` */

insert  into `gm_user_info`(`id`,`uid`,`avatar`,`qq`,`birthday`,`info`,`create_time`,`update_time`) values (1,1,'/uploads/avatar/20190924/20190924111752_666239.jpg','2992088140','970761600','&lt;p&gt;\n	hi\n&lt;/p&gt;',1502781914,1502781914),(2,2,'/uploads/avatar/20170815/20170815163031_249551.jpg','','','',1502782875,1502782875),(3,3,'/uploads/avatar/20170815/20170815163042_582354.jpg','','','',1502785283,1502785283),(4,4,'/uploads/avatar/20170815/20170815163103_675106.jpg','','','',1502785316,1502785316),(5,5,'/uploads/avatar/20170815/20170815162951_787194.jpg','','','',1502785706,1502785706);

/*Table structure for table `gm_ysbanner` */

DROP TABLE IF EXISTS `gm_ysbanner`;

CREATE TABLE `gm_ysbanner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `sorts` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ysbanner` */

insert  into `gm_ysbanner`(`id`,`title`,`pic`,`url`,`ctime`,`sorts`) values (1,'权利的游戏','/uploads/image/20191012/4cde7ee88049a4a48afcb7584dbd7ab9.jpg','https://www.baidu.com/',1570809600,1),(2,'冰与火之歌','/uploads/image/20191012/586cf7e9c57e746d55cfc2518c7b6734.jpg','https://www.baidu.com/',1570809600,2);

/*Table structure for table `gm_yscoop` */

DROP TABLE IF EXISTS `gm_yscoop`;

CREATE TABLE `gm_yscoop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `gm_yscoop` */

insert  into `gm_yscoop`(`id`,`title`,`pic`,`url`,`ctime`,`sorts`) values (1,'腾讯视频','/uploads/image/20191012/a932097d931fc3cf73b19de7259424bc.png','https://v.qq.com/',1570809600,1),(2,'优酷视频','/uploads/image/20191012/134c36fa1e975861838ddf826dd8a14d.jpg','https://www.youku.com/',1570809600,2),(3,'爱奇艺','/uploads/image/20191012/abf84ef78f192a9a9237ffeb362bff85.jpg','https://www.iqiyi.com/',1570809600,3),(4,'乐视','/uploads/image/20191012/c5d9eb1c87541fb699f2130096a7175b.jpg','http://www.le.com/',1570809600,4),(5,'芒果TV','/uploads/image/20191012/8500d86eeb670fc653e4e598f96c8724.jpg','https://www.mgtv.com/',1570809600,5);

/*Table structure for table `gm_ysdaohang` */

DROP TABLE IF EXISTS `gm_ysdaohang`;

CREATE TABLE `gm_ysdaohang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(50) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ysdaohang` */

insert  into `gm_ysdaohang`(`id`,`title`,`url`,`sorts`) values (1,'首页','http://ys.com/index/index/index',1),(2,'关于我们','http://ys.com/index/about/index',2),(3,'电影','http://ys.com/index/movie/index',3),(4,'电视剧','http://ys.com/index/tv/index',4),(5,'合作伙伴','http://ys.com/index/coop/index',5),(6,'经典影视','http://ys.com/index/jingdian/index',6),(7,'新闻资讯','http://ys.com/index/news/index',7);

/*Table structure for table `gm_ysjingdian` */

DROP TABLE IF EXISTS `gm_ysjingdian`;

CREATE TABLE `gm_ysjingdian` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(10) DEFAULT NULL,
  `type` varchar(10) DEFAULT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `pic` varchar(100) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ysjingdian` */

insert  into `gm_ysjingdian`(`id`,`title`,`type`,`text`,`pic`,`url`,`ctime`,`sorts`) values (4,'毒液','动作','毒液：致命守护者是漫威娱乐与哥伦比亚影业联合出品、索尼电影娱乐公司发行的科幻电影。于2018年10月5日在美国上映，2018年11月9日在中国大陆上映','/uploads/image/20191012/45970366c681272aa640bf5a7417bb8d.jpg','https://www.baidu.com/',1570982400,50),(2,'哥斯拉2','动作','哥斯拉2： 怪兽之王是由传奇影业华纳兄弟影片公司、华桦文化联合出品，迈克尔·道赫蒂执导，凯尔·钱德勒、米莉·博比·布朗、布莱德利·惠特福德、维拉·法梅加、渡边谦、莎莉·霍金斯、章子怡等人主演的一部科幻片。','/uploads/image/20191012/2dc87a96f45888e611810e6eca5e8fc6.jpg','https://www.baidu.com/',1570982400,50),(5,'X战警','动作','X战警：天启是一部由布莱恩·辛格执导，詹姆斯·麦卡沃伊、迈克尔·法斯宾德、詹妮弗·劳伦斯等主演的动作、冒险、科幻类型的电影，于2016年6月3日在中国上映。','/uploads/image/20191012/fd9c0d1efc82293480ccb80f875f57dd.jpg','https://www.baidu.com/',1570982400,50),(6,'蚁人','动作','前工程师斯科特由于劫富济贫进了监狱，出狱后的他为了争取看望女儿的权利又走上了盗窃之路。没想到一次意外的偷盗事件让他成为了新一代蚁人。','/uploads/image/20191012/fd05817e2d99ea5cf3c161b20d6a0073.png','https://www.baidu.com/',1570982400,50),(7,'摩天营救','动作','在香港市中心，世界上最高、结构最复杂的摩天大楼遭到破坏，危机一触即发。威尔·索耶的妻子萨拉和孩子们在98层被劫为人质，直接暴露在火线上。','/uploads/image/20191012/31c88ae4c596e5b6059b8ee52cfc141d.jpg','https://www.baidu.com/',1570982400,50);

/*Table structure for table `gm_ysmovie` */

DROP TABLE IF EXISTS `gm_ysmovie`;

CREATE TABLE `gm_ysmovie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci,
  `pic` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ysmovie` */

insert  into `gm_ysmovie`(`id`,`title`,`content`,`pic`,`url`,`ctime`,`sorts`) values (7,'正义联盟','同名长篇小说改编而成','/uploads/image/20191012/2d75e80b2681d8ed6d475835f3581100.jpg','https://www.baidu.com/',1570809600,50),(8,'反击美版','孤胆英雄上演真人版cs','/uploads/image/20191012/66768b62ca48f547360df273823d21ab.jpg','https://www.baidu.com/',1570809600,50),(6,'银河护卫队','是漫威出品的科幻动作电影','/uploads/image/20191012/13587b3499bfe9259a1cc40aea2c7335.jpg','https://www.baidu.com/',1570809600,50),(9,'惊奇队长','是美国安娜·波顿导演处女之作','/uploads/image/20191012/108a0c92b3263507921b3afa5f662f6d.jpg','https://www.baidu.com/',1570809600,50),(10,'心灵捕手','获得第70届奥斯卡金像奖','/uploads/image/20191012/b514b04f9a3aabb9d833058d4ac43986.jpg','https://www.baidu.com/',1570809600,50);

/*Table structure for table `gm_ysnews` */

DROP TABLE IF EXISTS `gm_ysnews`;

CREATE TABLE `gm_ysnews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `text` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `author` varchar(20) DEFAULT NULL,
  `url` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `utime` int(11) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ysnews` */

insert  into `gm_ysnews`(`id`,`title`,`text`,`pic`,`author`,`url`,`ctime`,`utime`,`sorts`) values (1,'那年那蝉那把剑','巍巍千年道门，三万门徒，三千弟子，三百真人，三十位大真人，掌教真人屹立于当世巅峰。\n\n浩浩百年皇室，千万子民，百万大军，十万官宦，百余红袍公卿，皇帝陛下坐拥这锦绣江山。\n\n真人们行走江湖，传述道祖至理。\n\n公卿们高踞庙堂，牧守天下众生。\n\n暗卫府潜于世内，为皇帝侦缉天下。\n\n镇魔殿隐于世外，为掌教镇压邪魔。\n\n儒家法家书生激辩王霸义利，道门佛门修士论道长生轮回。\n\n徐北游则是负剑而行。\n\n在他握剑的第一天，他就明白一个道理，握住夏蝉，未必是抓住夏天，但是握住了这把剑，便是握住了一个江湖。\n\n终有一天，他会一剑横行三千里，千军万马避白袍。','/uploads/image/20191012/18f166270c05e5e6c7369766038bd175.jpg','王晓亮','http://ys.com/index/select/index',1570896000,NULL,50),(2,'笔仙咒怨曝先导海报','电影笔仙咒怨讲述了黑心煤老板贪图女主人公外婆掌管的敬老院宅基地，男女主人公为帮外婆夺回古宅而与开发商对抗，进而发生的一系列骇人听闻的故事。\n\n\n\n故事在一所拥有600多年历史的古宅中实景拍摄，在当地流传着很多关于古宅的恐怖传说，阴森寂静的老宅，渗入现实的真实体验，给电影本身带来了更加诡异的恐怖基调。影片极具视觉冲击的画面、令人细思极恐的剧情、紧张激烈的节奏调控，恰如其分的展现了恐怖片的精髓，让观众在紧跟剧情的同时又为之一颤，营造了十足的恐怖氛围。\n\n\n\n有观众表示：从影片一开始就一直为主人公们捏一把汗，直到最后谜底揭开的一瞬间，悬着的心才放下来。\n\n\n\n影片的精良制作打造了惊悚恐怖电影的口碑之作，帧帧画面真实再现了中日经典灵异恐怖传说。 \n\n\n\n影片笔仙咒怨由北京卓迈影业有限公司、北京赢海投资有限公司、北京晟天文化传媒有限公司、上海小山影视文化传播有限公司、广东卓迈影业有限公司、牦牛映画北京文化传媒有限司、内蒙古卓迈影业有限公司、北京天天放送文化传播有限公司联合出品。 日前，影片正全国热映中，笔仙VS伽椰子，中日鬼王强强联合打造2017年度惊悚大戏。','/uploads/image/20191012/ef50a7d1b756060bafbe58a59814b091.jpg','王晓亮','http://ys.com/index/select/index',1570809600,NULL,50),(3,'完美世界','一粒尘可填海，一根草斩尽日月星辰，弹指间天翻地覆。     群雄并起，万族林立，诸圣争霸，乱天动地。问苍茫大地，谁主沉浮？！     一个少年从大荒中走出，一切从这里开始……','/uploads/image/20191012/ef50a7d1b756060bafbe58a59814b091.jpg','王晓亮','http://ys.com/index/select/index',1570809600,1571068800,1);

/*Table structure for table `gm_ystv` */

DROP TABLE IF EXISTS `gm_ystv`;

CREATE TABLE `gm_ystv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(20) DEFAULT NULL,
  `content` varchar(100) DEFAULT NULL,
  `pic` varchar(100) DEFAULT NULL,
  `url` varchar(50) DEFAULT NULL,
  `ctime` int(11) DEFAULT NULL,
  `sorts` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `gm_ystv` */

insert  into `gm_ystv`(`id`,`title`,`content`,`pic`,`url`,`ctime`,`sorts`) values (1,'我的真朋友','杨颖演的不错','/uploads/image/20191012/eba2191e930aa10580ffe11acfd6217c.jpg','https://www.baidu.com/',1570809600,50),(2,'陆战之王','还行吧','/uploads/image/20191012/a66fa81127098d3295e5788dc3719427.jpg','https://www.baidu.com/',1570809600,50),(3,'现代启示录','真的难看','/uploads/image/20191012/7ce6e3cc386af165b4edf97d0d6afa39.jpg','https://www.baidu.com/',1570809600,50);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
