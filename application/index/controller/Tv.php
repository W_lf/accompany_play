<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class Tv extends Controller
{
	public function index()
	{
		$parent = ['id' => '0'];
        $this->assign('parent', $parent);

		$tv=Db::name('ystv')->order('sorts asc')->select();//电视剧
        $this->assign('tv',$tv);
        $dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);

        return $this->fetch();//这里  都return了
	}

}


?>