<?php
namespace app\admin\validate;

use think\Validate;

class Ysjingdian extends Validate
{
    protected $rule = [//require是必填
        'title' => 'require',
        'text' => 'require',
        'tid' => 'require',
        'pic' => 'require',
        'url' => 'require',
        'ctime' => 'require',
        'sorts' => 'require|integer|>=:1',//integer是整数 后面是大于等于1的意思 以后排序字段最好用sorts
    ];

    protected $message = [
        'title' => '{%title_val}',
        'text' => '{%text_val}',
        'tid' => '{%type_val}',

        'pic' => '{%pic_val}',
        'url' => '{%url_val}',
        'ctime' => '{%ctime_val}',
        'sorts' => '{%stor_val}',
    ];

    protected $scene = [
        /*对应这个**/'add'   => ['title', 'tid', 'text', 'pic', 'url', 'ctime', 'sorts'],//意思就是点击增加时候 走验证类 验证这个 里面有这四个字段 然后去找到 上面的$rule去验证规则 验证不过的提示$message
        'edit'  => ['title', 'tid', 'text', 'pic', 'url', 'ctime', 'sorts'],
        'status' => ['status'],
    ];
}