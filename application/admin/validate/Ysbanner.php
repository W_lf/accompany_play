<?php
namespace app\admin\validate;

use think\Validate;

class Ysbanner extends Validate
{
    protected $rule = [
        'title' => 'require',
        'pic' => 'require',
        'ctime' => 'require',
        'url' => 'require',//require是必填
        'sorts' => 'require|integer|>=:1',//integer是整数 后面是大于等于1的意思 以后排序字段最好用sorts
    ];

    protected $message = [
        'title' => '{%title_val}',
        'pic' => '{%pic_val}',
        'ctime' => '{%ctime_val}',
        'url' => '{%url_val}',
        'sorts' => '{%stor_val}',
    ];

    protected $scene = [
        /*对应这个**/'add'   => ['title', 'url', 'pic', 'ctime', 'sorts'],//意思就是点击增加时候 走验证类 验证这个 里面有这四个字段 然后去找到 上面的$rule去验证规则 验证不过的提示$message
        'edit'  => ['title', 'url', 'pic', 'ctime', 'sorts'],
        'status' => ['status'],
    ];
}