<?php
namespace app\admin\validate;

use think\Validate;

class Ystype extends Validate
{
    protected $rule = [//require是必填
        'type' => 'require',
        'ctime' => 'require',
        'utime' => 'require',
       
    ];

    protected $message = [
        'type' => '{%type_val}'
        'utime' => '{%utime_val}',
        'ctime' => '{%ctime_val}',
    ];

    protected $scene = [
        /*对应这个**/'add'   => ['type','ctime', 'utime'],//意思就是点击增加时候 走验证类 验证这个 里面有这四个字段 然后去找到 上面的$rule去验证规则 验证不过的提示$message
        'edit'  => ['type','ctime', 'utime'],
        'status' => ['status'],
    ];
}