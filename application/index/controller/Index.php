<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class Index extends Controller
{
    public function _initialize(){
        parent::_initialize();
    }
    
    public function index()
    {
        $parent = ['id' => '0'];
        $this->assign('parent', $parent);

        $dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);

        $banner=Db::name('ysbanner')->order('sorts asc')->select();//banner图
        $this->assign('banner',$banner);

        $coop=Db::name('yscoop')->order('sorts asc')->select();//合作伙伴
        $this->assign('coop',$coop);

        $movie=Db::name('ysmovie')->order('sorts asc')->select();//电影
        $this->assign('movie',$movie);

        $tv=Db::name('ystv')->order('sorts asc')->select();//电视剧
        $this->assign('tv',$tv);

        $jd=Db::name('ysjingdian')->join('gm_ysjdtype','gm_ysjingdian.tid=gm_ysjdtype.id')->field('gm_ysjingdian.id,gm_ysjingdian.title,gm_ysjingdian.text,gm_ysjingdian.url,gm_ysjingdian.pic,gm_ysjingdian.ctime,gm_ysjingdian.sorts,gm_ysjdtype.type')->order('sorts asc')->select();//经典影视
        $this->assign('jd',$jd);

         $news=Db::name('ysnews')->order('sorts asc')->select();//新闻喜讯
        $this->assign('news',$news);


        return $this->fetch();
    }
    
    public function newarc($page){
        $archive = new \app\index\model\Archive();
        $typeid = 1;
        $typeidStr = cache('ARCTYPE_ARR_'.$typeid);
        if (!$typeidStr){
            $arctype = new \app\index\model\Arctype();
            $arctype::$allChild = array();   //初始化无限子分类数组
            $typeidArr = $arctype->allChildArctype($typeid);
            $typeidStr = implode(',', $typeidArr);
            cache('ARCTYPE_ARR_'.$typeid, $typeidStr);
        }
        $where['status'] = 1;
        $where['typeid'] = ['in', $typeidStr];
        $dataList = $archive->where($where)->order('id desc')->page($page.', 5')->select();
        
        foreach ($dataList as $k => $val){
            $flag_arr = explode(',', $val['flag']);
            if(in_array('j',$flag_arr) && !empty($val['jumplink'])){
                $dataList[$k]['arcurl'] = $val['jumplink'];
            }else{
                $dataList[$k]['arcurl'] = url('detail/'.$val->arctype->dirs.'/'.$val['id']);
            }
        }
        
        $this->assign('dataList', $dataList);
        return $this->fetch('inc/new_arc');
    }
}
