<?php
namespace app\admin\controller;

use think\Controller;
use app\admin\model\Pwhistory as Pwhistorys;

/**
 * 
 */
class Pwhistory extends Common//定义一个Pwfriend类继承common
{
	
	private $cModel;   //当前控制器关联模型，cmodel属性
    
    public function _initialize()//先使用这个方法
    {
        parent::_initialize();
        $this->cModel = new Pwhistorys;   //别名：避免与控制名冲突
    }
    
    public function index()
    {
        $where = [];
        if (input('get.search')){   //判断get传过来的值有没有seach值
            $where['name|mod'] = ['like', '%'.input('get.search').'%'];//
        }
        if (input('get._sort')){//判断get传过来的值有没有sort值 
            $order = explode(',', input('get._sort'));//把数组拆分成字符串
            $order = $order[0].' '.$order[1];//字符串连接
        }else{
            $order = 'id asc';//按照id进行排序 从小到大
        }
        $dataList = $this->cModel->where($where)->order($order)->paginate('', false, page_param());
        $this->assign('dataList', $dataList);//
        return $this->fetch();
    }
    
    public function create()
    {
        if (request()->isPost()){//判断是否有post提交
            $data = input('post.');//把所有的值接受过来
            $result = $this->cModel->validate(CONTROLLER_NAME.'.add')->allowField(true)->save($data);
            if ($result){
                return ajaxReturn(lang('action_success'), url('index'));
            }else{
                return ajaxReturn($this->cModel->getError());
            }
        }else{
            return $this->fetch('edit');
        }
    }
    
    public function edit($id)
    {
        if (request()->isPost()){
            $data = input('post.');
            if(in_array($data['id'], ['20','21','22'])){
                return ajaxReturn(lang('not_edit'));
            }
            if (count($data) == 2){
                foreach ($data as $k =>$v){
                    $fv = $k!='id' ? $k : '';
                }
                $result = $this->cModel->validate(CONTROLLER_NAME.'.'.$fv)->allowField(true)->save($data, $data['id']);
            }else{
                $result = $this->cModel->validate(CONTROLLER_NAME.'.edit')->allowField(true)->save($data, $data['id']);
            }
            if ($result){
                return ajaxReturn(lang('action_success'), url('index'));
            }else{
                return ajaxReturn($this->cModel->getError());
            }
        }else{
            $data = $this->cModel->get($id);
            $this->assign('data', $data);
            return $this->fetch();
        }
    }
    
    public function delete()
    {
        if (request()->isPost()){
            $id = input('id');
            if (isset($id) && !empty($id)){
                $id_arr = explode(',', $id);
                if(in_array(20, $id_arr) || in_array(21, $id_arr) || in_array(22, $id_arr)){
                    return ajaxReturn(lang('not_edit'));
                }
                $where = [ 'id' => ['in', $id_arr] ];
                $result = $this->cModel->where($where)->delete();
                if ($result){
                    return ajaxReturn(lang('action_success'), url('index'));
                }else{
                    return ajaxReturn($this->cModel->getError());
                }
            }
        }
    }
}

?>