<?php
namespace app\admin\controller;

use think\Controller;
use app\admin\model\Ysnews as Ysnewss;

class Ysnews extends Common
{
    private $cModel;   //当前控制器关联模型
    
    public function _initialize()
    {
        parent::_initialize();
        $this->cModel = new Ysnewss;   //别名：避免与控制名冲突
    }
    
    public function index()
    {
        $where = [];
        if (input('get.search')){  //判断get传过来的值有没有seach值
            $where['title|text'] = ['like', '%'.input('get.search').'%'];
        }
        if (input('get._sort')){
            $order = explode(',', input('get._sort'));//把数组拆分成字符串
            $order = $order[0].' '.$order[1];//字符串连接
        }else{
            $order = 'sorts asc,id asc';//按照排序，id进行排序 从小到大
        }
        $dataList = $this->cModel->where($where)->order($order)->paginate('', false, page_param());
        $this->assign('dataList', $dataList);
        return $this->fetch();
    }
    
    public function create()
    {
        if (request()->isPost()){
            $data = input('post.');

            $data['ctime'] = strtotime($data['ctime']);//将英文文本日期时间解析为 Unix 时间戳
            $data['utime'] = strtotime($data['utime']);//将英文文本日期时间解析为 Unix 时间戳
            $result = $this->cModel->validate(CONTROLLER_NAME.'.add')->allowField(true)->save($data);//这个 CONTROLLER_NAME这是控制器名 add方法 对应相应的验证类
            if ($result){
                return ajaxReturn(lang('action_success'), url('index'));
            }else{
                return ajaxReturn($this->cModel->getError());
            }
        }else{
            return $this->fetch('edit');
        }
    }
    
    public function edit($id)
    {
        if (request()->isPost()){
            $data = input('post.');
            $data['ctime'] = strtotime($data['ctime']);
            $data['utime'] = strtotime($data['utime']);
            if (!$id){
                foreach ($data as $k =>$v){
                    $fv = $k!='id' ? $k : '';
                }

                $result = $this->cModel->validate(CONTROLLER_NAME.'.'.$fv)->allowField(true)->save($data, $data['id']);
            }else{
                $result = $this->cModel->validate(CONTROLLER_NAME.'.edit')->allowField(true)->save($data, $data['id']);//这里的是验证edit方法
            }
            if ($result){
                return ajaxReturn(lang('action_success'), url('index'));
            }else{
                return ajaxReturn($this->cModel->getError());
            }
        }else{
            $data = $this->cModel->get($id);
            $this->assign('data', $data);
            return $this->fetch();
        }
    }
    
    public function delete()
    {
        if (request()->isPost()){
            $id = input('id');
            if (!empty($id)){
                $id_arr = explode(',', $id);
                $where = [ 'id' => ['in', $id_arr] ];
                $result = $this->cModel->where($where)->delete();
                if ($result){
                    return ajaxReturn(lang('action_success'), url('index'));
                }else{
                    return ajaxReturn($this->cModel->getError());
                }
            }
        }
    }
}