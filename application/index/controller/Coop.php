<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class Coop extends Controller
{
	public function index()
	{
		$parent = ['id' => '0'];
        $this->assign('parent', $parent);

        $dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);
		$coop=Db::name('yscoop')->order('sorts asc')->select();//合作伙伴
        $this->assign('coop',$coop);

        return $this->fetch();//这里  都return了
	}

}


?>