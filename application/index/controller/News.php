<?php
namespace app\index\controller;
use think\Db;
use think\Controller;

class News extends Controller
{
	public function index()
	{
		$parent = ['id' => '0'];
        $this->assign('parent', $parent);
        $dh=Db::name('ysdaohang')->order('sorts asc')->select();//导航
        $this->assign('dh',$dh);
        
		$news=Db::name('ysnews')->order('sorts asc')->select();//新闻喜讯
        $this->assign('news',$news);

        return $this->fetch();//这里  都return了
	}

}


?>